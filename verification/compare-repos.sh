#!/bin/bash

repo=$1
svn_repos=file:///mnt/git-data/svn
git_repos=/git
tmp=/tmp

# Log everthing to this file
exec > $repo-verify.log 2>&1

function my_diff () {
  diff --unified=0 $1 $2 | sed -e '1,+2d; s/^\([+-]\)/\1 /'
  cat $1 $2 | sort | uniq -d | while read l;
    do echo ": $l";
  done
}

function compare()
{
    svn_path=$1
    git_ref=$2

    echo comparing $svn_path and $git_ref

    svn co -q $svn_repos/$repo/$svn_path $tmp/$repo-svn
    (
	cd $tmp/$repo-svn
	svn ls -R | sed -e \\:/$:d | sort > $tmp/$repo-svn-files

	# Unexpand stupid svn keywords
	cat $tmp/$repo-svn-files |
	xargs sed -i -e 's/\$\(Id\|Revision\|Author\|Date\|Log\)\:.*\$/$\1$/'
    )

    git clone -q $git_repos/$repo.git $tmp/$repo-git
    (
	cd $tmp/$repo-git
	git checkout -q -b xxx master
	git reset -q --hard $git_ref
	git ls-files | sort > $tmp/$repo-git-files

	# Unexpand stupid svn keywords
	cat $tmp/$repo-git-files |
	xargs sed -i -e 's/\$\(Id\|Revision\|Author\|Date\|Log\)\:.*\$/$\1$/'
    )

    my_diff $tmp/$repo-svn-files $tmp/$repo-git-files | while read k f; do 
	case $k in
	    :)
	    if test -h $tmp/$repo-svn/$f -a -h $tmp/$repo-git/$f; then
		test $(readlink $tmp/$repo-svn/$f) == $(readlink $tmp/$repo-git/$f) ||
		echo symlink $f does not match
	    else 
		diff -u -b $tmp/$repo-svn/$f $tmp/$repo-git/$f
	    fi
	    ;;
	    +)
	    echo extra file in git: $f
	    ;;
	    -)
	    echo file missing in git: $f
	    ;;
	esac
    done

    rm -rf  $tmp/$repo-git $tmp/$repo-svn $tmp/$repo-svn-files $tmp/$repo-git-files
}

rm -rf  $tmp/$repo-git $tmp/$repo-svn

function compare_tags()
{
    echo === TAGS

    svn ls $svn_repos/$repo/tags | sed -e s:/$:: | sort > $tmp/$repo-svn-tags
    GIT_DIR=$git_repos/$repo.git git tag | sort > $tmp/$repo-git-tags

    my_diff $tmp/$repo-svn-tags $tmp/$repo-git-tags | while read k t; do 
	case $k in
	    :) compare tags/$t $t ;;
	    +) echo extra tag not in svn: $t ;;
	    -) echo svn tag not in git: $t ;;
	esac
    done

    rm $tmp/$repo-svn-tags $tmp/$repo-git-tags
}

echo === MASTER
compare trunk master

compare_tags

