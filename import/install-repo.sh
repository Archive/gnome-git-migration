#!/bin/sh

export GIT_DIR=$1
DST=$2

git config hooks.emailprefix ""
git config hooks.mailinglist svn-commits-list@gnome.org

ln -sf /home/admin/gitadmin-bin/gnome-pre-receive $GIT_DIR/hooks/pre-receive
ln -sf /home/admin/gitadmin-bin/gnome-post-receive $GIT_DIR/hooks/post-receive

git config core.sharedrepository 1
mv $GIT_DIR $DST
