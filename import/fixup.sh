#!/bin/sh

export GIT_DIR=$1

git tag | while read tag; do
    case $(git cat-file -t $tag) in

	commit)

	    echo Updating light-weight tag $tag

	    # First figure out which commit to tag.  Some svn tags
	    # have no changes, but record some files as changed
	    # regardless.  We see if the tree of the tagged commit is
	    # identical to the tree of the parent commit and in that
	    # case we move the tag back to the parent.  See for
	    # example most of the recent vino tags.

	    tree=$(git cat-file -p $tag | sed -ne "s/tree //p")
	    parent=$tag^
	    parent_tree=$(git cat-file -p $parent | sed -ne "s/tree //p")
	    if [ $tree == $parent_tree ]; then
		commit=$parent
	    else
		commit=$tag
	    fi

	    cat <<EOF | git mktag > fixup-tag-ref
object $(git rev-parse $commit)
type commit
tag $tag
$(git cat-file -p $commit | sed -ne "s/author /tagger /p")

$(git cat-file -p $commit | sed -e '1,+4d')
EOF
	    git update-ref refs/tags/$tag $(cat fixup-tag-ref)

	    ;;

	tag)
	    ;;

    esac
done

git filter-branch -f --commit-filter ${PWD}'/gnome-edit-changelog $@' --tag-name-filter cat -- --all

# Delete backup refs created by filter-branch
git for-each-ref --format="%(refname)" refs/original | while read ref; do 
    git update-ref -d $ref;
done

rm -rf $GIT_DIR/refs/original

git repack -a -d -f --window=50 --depth=20
