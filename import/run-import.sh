#!/bin/sh

if [ -z "$1" ]; then
  echo usage: $0 REPO
  exit 1
fi

repo=$1

# Hardcoded paths go here
cvs_repos=/cvs/gnome
svn_repos=/mnt/git-data/svn
fast_export=/home/users/krh/svn-all-fast-export/svn-all-fast-export
parsecvs=/home/users/krh/parsecvs/parsecvs

function write_submodule_config () {
    r=${1/\//-}
    cat <<EOF
create repository $r
end repository

match ^/$1/branches/([^/+]+)/
repository $r
branch \1
end match

match ^/$1/branches/([^/+]+)
repository $r
branch \1
end match

match ^/$1/branches/
action recurse
end match

match ^/$1/tags/([^/+]+)/
repository $r
branch refs/tags/\1
end match

match ^/$1/tags/([^/+]+)
repository $r
branch refs/tags/\1
end match

match ^/$1/tags/
action recurse
end match

match ^/$1/trunk/
repository $r
branch master
end match

match ^/$1/trunk
repository $r
branch master
end match

match ^/$1/$
action recurse
end match

match ^/$1$
action ignore
end match

EOF
}

function write_config () {
    cat <<EOF
create repository $repo

end repository

match ^/branches/([^/+]+)/
repository $repo
branch \1
end match

match ^/branches/([^/+]+)
repository $repo
branch \1
end match

match ^/tags/([^/+]+)/
repository $repo
branch refs/tags/\1
end match

match ^/tags/([^/+]+)
repository $repo
branch refs/tags/\1
end match

match ^/trunk/
repository $repo
branch master
end match

match ^/trunk
repository $repo
branch master
end match

match ^/(libbonobomm|libbonobouimm|libgnomeprintmm|libgnomeprintuimm)
replace /deprecated/\1
end match

match ^/deprecated
action ignore
end match

EOF

    for m in $@; do
	write_submodule_config $m
    done

    cat <<EOF 

match ^/(temp|banshee|desktop|bigboard|dumbhippo|plugins)
replace /trunk/git-migration-\1
end match

match ^/packaging
replace /branches/packaging
end match

# wtf gtkmathview...
match ^/releases/([^/]+)
replace /tags/\1
end match

match ^/([^/]+)
replace /tags/git-migration-\1
end match

EOF
}

function run_fast_export () {
    write_config $@ > config

    if [ -d $cvs_repos/$cvs_repo ]; then
	start=$(svn info file://$svn_repos/$repo -r {2006-12-30}  | sed -ne 's/^Revision: //p')
	start=$(($start + 1))
	resume="-resume-from $start"
    fi

    echo $fast_export $resume -identity-map gnome-all-usermap config $svn_repos/$repo

    $fast_export $resume -identity-map gnome-all-usermap config $svn_repos/$repo
}

# Log everthing to this file
exec > $repo-import.log 2>&1

cvs_repo=$repo

case $repo in

    seahorse)
	submodules=seahorse-plugins
	;;

    gnomemm)
	submodules="clutter-box2dmm clutter-cairomm clutter-gstreamermm clutter-gtkmm cluttermm cluttermm_tutorial gconfmm geglmm gnome-vfsmm gnomemm_docs gnomemm_hello goocanvasmm giomm gstreamermm gtkmm_hello libgdamm libglademm libgnomecanvasmm libgnomedbmm libgnomemm libgnomeuimm libpanelappletmm"
	;;

    gdm)
	cvs_repo=gdm2
	;;
esac

for m in $repo $submodules; do
    r=${m/\//-}
    echo Deleting existing git dir $r.git
    rm -rf $r.git
done

for m in $repo $submodules; do
    GIT_DIR=$m.git git init --shared
done

echo Starting import of $repo [$(date)]

if [ -d $cvs_repos/$cvs_repo ]; then
    echo Running cvs export for $repo [$(date)]
    find $cvs_repos/$cvs_repo -name '*,v' | GIT_DIR=$repo.git $parsecvs
else
    echo No cvs history [$(date)]
fi

for m in $submodules; do
    if [ -d $cvs_repos/$cvs_repo/$m ]; then
	echo Running cvs export for $m [$(date)]
	find $cvs_repos/$cvs_repo/$m -name '*,v' | GIT_DIR=$m.git $parsecvs
    fi
done

if [ -d $svn_repos/$repo ]; then
    echo Running svn export [$(date)]
    run_fast_export $submodules
else
    echo No svn history [$(date)]
fi

echo All done [$(date)]
